export interface Item {
  title: string[]|string,
  description: string[]|string,
  votes: number,
  id?: string,
  createdOn?: number,
  createdBy?: string
}

export interface Bulletin {
  title: string[]|string,
  description: string[]|string,
  verified: boolean,
  createdOn: number,
  createdBy?: string,
  createdByName?: string,
  id: string,
  placeholder: boolean
}

export interface UserRecord {
  uid: string,
  email?: string,
  name: string,
  nic: string,
  verified: boolean,
  votedFor: {nomination: string, election: string}[],
  admin: boolean
}

export interface AuthError {
  message: string,
  code: string,
  email: string,
  name: string
}

export interface LanguageStrings {
  [key: string]: string[]
}

export interface Video {
  code: any,
  createdOn: number,
  createdBy: string
}

export interface Photo {
  url: string,
  createdByName: string,
  createdOn: number,
  createdBy: string
}
