import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemocraticComponent } from './apps/democratic/democratic.component'
import { MediaComponent } from './apps/media/media.component'
import { HomeComponent } from './components/home/home.component'
import { ContributeComponent } from './components/contribute/contribute.component'
import { EmbedComponent } from './apps/media/components/embed/embed.component'
import { BulletinsComponent } from './apps/bulletins/bulletins.component'
import { ElectionComponent } from './apps/election/election.component';
import { GalleryComponent } from './apps/gallery/gallery.component';
import { ProposeComponent } from './components/propose/propose.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'polls/:name', component: DemocraticComponent},
  {path: 'news', component: MediaComponent},
  {path: 'news/embed', component: EmbedComponent},
  {path: 'discussions', component: MediaComponent},
  {path: 'discussions/embed', component: EmbedComponent},
  {path: 'contribute', component: ContributeComponent},
  {path: 'bulletins', component: BulletinsComponent},
  {path: 'polls', component: ElectionComponent},
  {path: 'gallery', component: GalleryComponent},
  {path: 'propose', component: ProposeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
