import { Injectable, isDevMode } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatastoreService {

  public language = new BehaviorSubject<number>(0);
  public user = new BehaviorSubject<any>(undefined);

  constructor() {}

  setLanguage(language: number) {
    this.language.next(language);
  }

  getLanguage() {
    return this.language;
  }

  setUser(user: any) {
    if(user) {
      console.log(user.email, user.admin);
    }
    this.user.next(user);
  }

  getUser() {
    return this.user;
  }
}
