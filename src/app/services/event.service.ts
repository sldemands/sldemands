import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

interface Event {
  tag: string,
  emitter: Subject<any>
}

@Injectable({
  providedIn: 'root'
})
export class EventService {

  events: Event[] = [];

  constructor() {
  }

  emitEvent(tag: string, value?: any) {
    let event = this.events.find((event: Event) => (tag == event.tag));
    if(event) {
      event.emitter.next(value);
    }
  }

  registerEvent(tag: string) {
    let subject: Subject<any>;
    let event = this.events.find((event: Event) => (tag == event.tag));

    if(event) {
      subject = event.emitter;
    } else {
      subject = new Subject<any>();
      this.events.push({
        tag: tag,
        emitter: subject
      });
    }

    return subject;
  };

}
