import { Injectable } from '@angular/core';
import { getAuth } from "firebase/auth";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { DatabaseService } from "./database.service";
import { DatastoreService } from "./datastore.service";
import { UserService } from './user.service';
import { v4 as uuid } from 'uuid';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  storage: any;

  constructor(
    private dbService: DatabaseService,
    private userService: UserService,
    private datastore: DatastoreService
  ) { }

  async uploadToGallery(file: File) {
    let user = await firstValueFrom(this.datastore.getUser());
    if(user) {
      let name = uuid();
      const storageRef = ref(this.storage, 'gallery/'+ name);

      await uploadBytes(storageRef, file);

      await this.dbService.addPhoto({
        url: '/gallery/'+ name,
        createdBy: user.uid,
        createdByName: user.name,
        createdOn: (new Date()).getTime()
      });
    } else {
      console.log('not signed in');
    }
  }

  async uploadToProposals(file: File) {
    let user = await firstValueFrom(this.datastore.getUser());

    if(user) {
      let name = uuid();
      const storageRef = ref(this.storage, 'proposals/'+ name);

      await uploadBytes(storageRef, file);

      return {
        url: '/proposals/'+ name,
        name: file.name,
        createdBy: user.uid,
        createdByName: user.name,
        createdOn: (new Date()).getTime()
      };
    } else {
      console.log('not signed in');
      return undefined;
    }
  }

  async getURL(url: string) {
    return getDownloadURL(ref(this.storage, url));
  }
}
