import { Injectable } from '@angular/core';
import { getAuth, signInWithEmailAndPassword, signInWithRedirect, FacebookAuthProvider, User, onAuthStateChanged, createUserWithEmailAndPassword } from "firebase/auth";
import { DatabaseService } from '../services/database.service';
import { EventService } from '../services/event.service';
import { collection, query, where, getDocs, doc, setDoc, Timestamp } from "firebase/firestore";
import { AuthError, UserRecord } from '../types/types';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private dbService: DatabaseService,
    private eventService: EventService
  ) { }

  async facebookSigninRedirect() {
    const auth = getAuth();
    const provider = new FacebookAuthProvider();
    signInWithRedirect(auth, provider);
  }

  async signUpWithEmail(email: string, password: string): Promise<string> {
    const auth = getAuth();
    return new Promise((resolve, reject) => {
      createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        resolve(userCredential.user.uid);
      })
      .catch((error) => {
        this.dbService.createError(error as any, 'UserService:signUpWithEmail');
        reject(error.toString());
      });
    })
  }

  async signInWithEmail(email: string, password: string): Promise<string> {
    const auth = getAuth();
    return new Promise((resolve, reject) => {
      signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        resolve('signed in');
      })
      .catch((error) => {
        this.dbService.createError(error as any, 'UserService:signInWithEmail');
        reject(error);
      });
    })
  }

}
