import { Injectable, isDevMode } from '@angular/core';
import { getFunctions, httpsCallable } from 'firebase/functions';
const env  = require('../../env.json');

function DD(data: any) { // Dev Data
  return {...data, dev: (!env.force_prod && (env.force_dev || isDevMode()))}
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  public http: any;

  constructor() { }

  async updateText(args: any) {
    return httpsCallable(this.http, 'updatetext')(DD(args));
  }

  async createElection(election: any) {
    return httpsCallable(this.http, 'addelection')(DD(election));
  }

  async createProposal(proposal: any) {
    return httpsCallable(this.http, 'addproposal')(DD(proposal));
  }

  async createNomination(nomination: any) {
    return httpsCallable(this.http, 'addnomination')(DD(nomination));
  }

  async createBulletin(bulletin: any) {
    return httpsCallable(this.http, 'addbulletin')(DD(bulletin));
  }

  async createComment(comment: any) {
    return httpsCallable(this.http, 'addcomment')(DD(comment));
  }

  async vote(nominationId: any) {
    return httpsCallable(this.http, 'addvote')(DD(nominationId));
  }

  async deleteNomination(nominationId: any) {
    return httpsCallable(this.http, 'deletenomination')(DD(nominationId));
  }
}
