import { Injectable, isDevMode } from '@angular/core';
import { collection, query, where, getDocs, getDoc, doc, setDoc, addDoc, updateDoc, deleteDoc, Timestamp, increment, arrayUnion, arrayRemove, orderBy, limit, onSnapshot } from "firebase/firestore";
import { Item, UserRecord, Video, Bulletin, Photo } from '../types/types';
import { getAuth, signInWithPopup, GoogleAuthProvider, User } from "firebase/auth";
import { Observable, from } from 'rxjs';
const env  = require('../../env.json');

function DBN(name: string) { // Data Base Name
  return name + ((!env.force_prod && (env.force_dev || isDevMode()))? '-dev': '');
}

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  public db: any;

  constructor() { }

  async getRecord(collection: string, id: string) {
    try {
      let record = await getDoc(doc(this.db, DBN(collection), id));
      return record.data();

    } catch (error) {
      this.createError(error as any, 'DBService:getRecord');
      return undefined;
    }
  }

  getCommentsListener(id: string) {
    return new Observable<any[]>(subscriber => {
      let unsubscribe = ()=>{};
      try {
        const c = collection(this.db, DBN('Comments'));
        const q = query(c, where('parentId', '==', id), orderBy("createdOn"));
         unsubscribe = onSnapshot(q, (qs) => {
          const comments: any = [];
          qs.forEach((doc) => {
            comments.push(doc.data());
          });
          subscriber.next(comments);
        });
      } catch (error) {
        this.createError(error as any, 'DBService:getCommentsListener');
      }
      return () => {
        unsubscribe();
      };
    });
  }

  async getProposals() {
    try {
      const c = collection(this.db, DBN('Proposals'));
      const q = query(c, orderBy("createdOn", "desc"));
      const qs = await getDocs(q);
      const items: any[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data());
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getPhotos');
      return [];
    }
  }

  async addPhoto(item: Photo) {
    try {
      const docRef = doc(collection(this.db, DBN("Gallery")));
      await setDoc(
        docRef,
        {
          ...item,
          id: docRef.id
        }
      );
    } catch (error) {
      this.createError(error as any, 'DBService:addPhoto');
    }
  }

  async getPhotos() {
    try {
      const c = collection(this.db, DBN('Gallery'));
      const q = query(c, orderBy("createdOn", "desc"));
      const qs = await getDocs(q);
      const items: Photo[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Photo);
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getPhotos');
      return [];
    }
  }

  async getTopPhoto() {
    try {
      const c = collection(this.db, DBN('Gallery'));
      const q = query(c, orderBy("createdOn", "desc"), limit(1));
      const qs = await getDocs(q);
      const items: Photo[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Photo);
        }
      });
      return items[0];
    } catch (error) {
      this.createError(error as any, 'DBService:getTopPhoto');
      return undefined;
    }
  }

  async getElection(id: string) {
    try {
      let election = await getDoc(doc(this.db, DBN("Elections"), id));
      return election.data();
    } catch (error) {
      this.createError(error as any, 'DBService:getElection');
      return undefined;
    }
  }

  async getElections() {
    try {
      const c = collection(this.db, DBN("Elections"));
      const qs = await getDocs(c);
      const items: any[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data());
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getElections');
      return [];
    }
  }

  async createLog(event: string) {
    try {
      const docRef = await addDoc(collection(this.db, DBN("Logs")), {
        event: event,
        signedin: getAuth().currentUser != null,
        time: (new Date()).getTime()
      });
    } catch (error) {
      this.createError(error as any, 'DBService:createLog');
    }
  }

  async getBulletins() {
    try {
      const qs = await getDocs(collection(this.db, DBN("Bulletin")));
      const items: Bulletin[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Bulletin);
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getBulletins');
      return [];
    }

  }

  async getTopBulletins(count: number) {
    try {
      const c = collection(this.db, DBN('Bulletin'));
      const q = query(c, orderBy("createdOn", "desc"), where("verified", "==", true), limit(count));
      const qs = await getDocs(q);
      const items: Bulletin[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Bulletin);
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getTopBulletin');
      return [];
    }
  }

  async getTopNews() {
    try {
      const c = collection(this.db, DBN('News'));
      const q = query(c, orderBy("createdOn", "desc"), where("verified", "==", true), limit(1));
      const qs = await getDocs(q);
      const items: Video[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Video);
        }
      });
      return items[0];
    } catch (error) {
      this.createError(error as any, 'DBService:getTopNews');
      return undefined;
    }
  }

  async getTopDiscussion() {
    try {
      const c = collection(this.db, DBN('Discussions'));
      const q = query(c, orderBy("createdOn", "desc"), where("verified", "==", true), limit(1));
      const qs = await getDocs(q);
      const items: Video[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Video);
        }
      });
      return items[0];
    } catch (error) {
      this.createError(error as any, 'DBService:getTopDiscussion');
      return undefined;
    }
  }

  async embedNewsVideo(item: any) {
    try {
      const docRef = doc(collection(this.db, DBN("News")));
      await setDoc(
        docRef,
        {
          ...item,
          id: docRef.id
        }
      )
    } catch (error) {
      this.createError(error as any, 'DBService:embedNewsVideo');
    }
  }

  async getNewsVideos() {
    try {
      const qs = await getDocs(collection(this.db, DBN("News")));
      const items: Video[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Video);
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getNewsVideos');
      return [];
    }
  }

  async embedDiscussionVideo(item: any) {
    try {
      const docRef = doc(collection(this.db, DBN("Discussions")));
      await setDoc(
        docRef,
        {
          ...item,
          id: docRef.id
        }
      )
    } catch (error) {
      this.createError(error as any, 'DBService:embedDiscussionVideo');
    }
  }

  async getDiscussionVideos() {
    try {
      const qs = await getDocs(collection(this.db, DBN("Discussions")));
      const items: Video[] = [];
      qs.docs.forEach(doc => {
        if(doc.id !== '0' && doc.exists()) {
          items.push(doc.data() as Video);
        }
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getDiscussionVideos');
      return [];
    }
  }

  async createError(error: any, caller: string) {
    try {
      if(isDevMode()) {
        console.error(caller, error);
        return;
      }
      const docRef = await addDoc(collection(this.db, DBN("Errors")), {
        message: error.message,
        code: error.code,
        caller: caller,
        signedin: getAuth().currentUser != null,
        time: (new Date()).getTime()
      });
    } catch (error) {
      console.log(`Couldn't save error:`, error);
    }
  }

  async getUserRecord(uid: string) {
    try {
      let userRecord = await getDoc(doc(this.db, DBN("Users"), uid));
      return userRecord.data();
    } catch (error) {
      this.createError(error as any, 'DBService:getUserRecord');
      return undefined;
    }
  }

  async checkUserExists(email: string) {
    try {
      let userRecord = await getDoc(doc(this.db, DBN("UserEmails"), email));
      return userRecord.exists()? userRecord.data()['provider']: undefined;
    } catch (error) {
      this.createError(error as any, 'DBService:checkUserExists');
      return undefined;
    }
  }

  async updateUserRecord(updates: any) {
    try {
      const authUser = getAuth().currentUser || {uid: ''};
      await updateDoc(doc(this.db, DBN("Users"), authUser.uid), updates);
      return true;
    } catch (error) {
      this.createError(error as any, 'DBService:updateUserRecord');
      return false;
    }
  }

  async createUserRecord(uid: string, name: string, email: string, nic: string) {
    try {
      let user: UserRecord = {
        uid: uid,
        name: name,
        email: email,
        nic: nic,
        verified: false,
        votedFor: [],
        admin: false
      }
      let authUser = getAuth().currentUser;
      let userEmail = {
        active: true,
        provider: authUser? authUser.providerData[0].providerId: 'null'
      }
      await setDoc(doc(this.db, DBN("Users"), uid), user, { merge: true });
      await setDoc(doc(this.db, DBN("UserEmails"), email), userEmail);
      return user;
    } catch (error) {
      this.createError(error as any, 'DBService:createUserRecord');
      return undefined;
    }
  }

  async getItems(election: string) {
    try {
      const c = collection(this.db, DBN('Nominations'));
      const q = query(c, where('election', '==', election ));
      const qs = await getDocs(q);
      const items: Item[] = [];
      qs.docs.forEach(doc => {
        items.push(doc.data() as Item);
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getItems');
      return [];
    }
  }

  async getItemsByVotes(election: string) {
    try {
      const c = collection(this.db, DBN('Nominations'));
      const q = query(c, where('election', '==', election ), orderBy("votes", "desc"));
      const qs = await getDocs(q);
      const items: Item[] = [];
      qs.docs.forEach(doc => {
        items.push(doc.data() as Item);
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getItemsByVotes');
      return [];
    }
  }

  async getYourItems(election: string) {
    try {
      const auth = getAuth();
      const c = collection(this.db, DBN('Nominations'));
      const q = query(c, where('election', '==', election), where("createdBy", "==", auth.currentUser? auth.currentUser.uid : ''));
      const qs = await getDocs(q);
      const items: Item[] = [];
      qs.docs.forEach(doc => {
        items.push(doc.data() as Item);
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getYourItems');
      return [];
    }
  }

  async getVotedItems(election: string) {
    try {
      let auth = getAuth();
      const c = collection(this.db, DBN('Nominations'));
      const q = query(c, where('election', '==', election), where('votedBy', 'array-contains', auth.currentUser?.uid ));
      const qs = await getDocs(q);
      const items: Item[] = [];
      qs.docs.forEach(doc => {
        items.push(doc.data() as Item);
      });
      return items;
    } catch (error) {
      this.createError(error as any, 'DBService:getVotedItems');
      return [];
    }
  }
}
