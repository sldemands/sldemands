import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseComponent } from './apps/democratic/components/base/base.component';
import { DemandsComponent } from './apps/democratic/components/demands/demands.component';
import { CreateComponent } from './apps/democratic/components/create/create.component';
import { DemandComponent } from './apps/democratic/components/demand/demand.component';
import { LoginComponent } from './components/login/login.component';
import { DemocraticComponent } from './apps/democratic/democratic.component';
import { HomeComponent } from './components/home/home.component';
import { CardComponent } from './components/card/card.component';
import { MediaComponent } from './apps/media/media.component';
import { EmbedComponent } from './apps/media/components/embed/embed.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { BulletinsComponent } from './apps/bulletins/bulletins.component';
import { BulletinComponent } from './components/bulletin/bulletin.component';
import { BulletinCreateComponent } from './apps/bulletins/components/bulletin-create/bulletin-create.component';
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ContributeComponent } from './components/contribute/contribute.component';
import { ElectionComponent } from './apps/election/election.component';
import { GalleryComponent } from './apps/gallery/gallery.component';
import { PhotoComponent } from './components/photo/photo.component';
import { CommentsComponent } from './components/comments/comments.component';
import { LanguageComponent } from './components/language/language.component';
import { ProgressComponent } from './components/progress/progress.component';
import { LoaderComponent } from './components/loader/loader.component';
import { PollComponent } from './components/poll/poll.component';
import { ProposalComponent } from './components/proposal/proposal.component';
import { EditableComponent } from './components/editable/editable.component';
import { DataeditComponent } from './components/dataedit/dataedit.component';
import { ProposeComponent } from './components/propose/propose.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    DemandsComponent,
    CreateComponent,
    DemandComponent,
    LoginComponent,
    DemocraticComponent,
    HomeComponent,
    CardComponent,
    MediaComponent,
    EmbedComponent,
    IframeComponent,
    BulletinsComponent,
    BulletinComponent,
    BulletinCreateComponent,
    ContributeComponent,
    ElectionComponent,
    GalleryComponent,
    PhotoComponent,
    CommentsComponent,
    LanguageComponent,
    ProgressComponent,
    LoaderComponent,
    PollComponent,
    ProposalComponent,
    EditableComponent,
    DataeditComponent,
    ProposeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatSelectModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularEditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
