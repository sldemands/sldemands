import { Component, OnInit, OnChanges, ViewChild, AfterViewInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.css']
})
export class IframeComponent implements OnInit, OnChanges {

  @ViewChild('embed')
  private element: ElementRef;
  @Input()
  data: any;
  @Input()
  collection: string;
  timeString: string;
  viewInitialized: boolean = false;

  constructor() { }

  ngAfterViewInit() {
    this.viewInitialized = true;
    if(this.data) this.updateVideo();

  }

  ngOnChanges() {
    if(this.viewInitialized && this.data) this.updateVideo();
  }

  updateVideo() {
    let clientWidth = this.element.nativeElement.clientWidth;
    var re1 = /width=\""?(\d+)"\"?/gi;
    let re1result = re1.exec(this.data.code);
    if(re1result)
      this.data.code = this.data.code.replace(re1result[0],'width="' + clientWidth + '"');
    var re2 = /width="?(\d+)"?/gi;
    let re2result = re2.exec(this.data.code);
    if(re2result)
      this.data.code = this.data.code.replace(re2result[0],'width=' + clientWidth);
    var re3 = /height=\""?(\d+)"\"?/gi;
    let re3result = re3.exec(this.data.code);
    if(re3result)
      this.data.code = this.data.code.replace(re3result[0],'height="300"');
    var re4 = /height="?(\d+)"?/gi;
    let re4result = re4.exec(this.data.code);
    if(re4result)
      this.data.code = this.data.code.replace(re4result[0],'height=300');

    this.element.nativeElement.innerHTML = this.data.code? this.data.code: '';
    let date = new Date(this.data.createdOn);
    this.data.createdOnString = date.toLocaleString('default', { month: 'short' })
                        + ' ' + date.getDate()
                        + ', ' + date.toTimeString().substring(0,9);
  }

  ngOnInit(): void {

  }

  onLoad() {
    console.log('loaded');

  }
}
