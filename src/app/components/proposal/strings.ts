import { LanguageStrings } from '../../types/types';

export const ProposalStrings: LanguageStrings = {
  proposalsTitle: [
    'මහජන අදහස් සඳහා විවෘත යෝජනා',
    'Proposals for public comments',
    'பொது கருத்துகளுக்கான முன்மொழிவுகள்'
  ],
  institutionName: [
    'ආයතනයේ නම',
    'Name of the institution',
    'நிறுவனத்தின் பெயர்',
  ],
  institutionDescription: [
    'ආයතනයේ විස්තරය',
    'About the organization',
    'அமைப்பு பற்றி'
  ],
  projectMotive: [
    'ව්‍යාපෘතියේ අරමුණ',
    'Purpose of the project',
    'திட்டத்தின் நோக்கம்'
  ],
  downloadProposal: [
    'යෝජනාවලිය බාගත කරගන්න',
    'Download the proposals',
    'முன்மொழிவுகளைப் பதிவிறக்கவும்'
  ],
  filesTitle: [
    'ව්‍යාපෘති ලියවිලි බාගත කරගන්න',
    'Download proposal documents',
    'முன்மொழிவு ஆவணங்களைப் பதிவிறக்கவும்'
  ]
}
