import { Component, OnInit, OnDestroy, OnChanges, Input } from '@angular/core';
import { ProposalStrings } from './strings';
import { DatastoreService } from '../../services/datastore.service';
import { DatabaseService } from '../../services/database.service';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-proposal',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.css']
})
export class ProposalComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  proposal: any;
  strings = ProposalStrings;
  language: number = 0;
  languageSubscription: any;
  files: any[] = [];
  minimized = true;
  minimizedFiles = true;

  constructor(
    private datastore: DatastoreService,
    private dbService: DatabaseService,
    private storage: StorageService
  ) {
    this.languageSubscription = datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
    this.updateReactiveData();
  }

  ngOnChanges(): void {
    this.updateReactiveData();
  }

  updateReactiveData() {
    if(!this.proposal || this.proposal.placeholder) return;

    if(this.proposal._motivePreview == undefined) this.proposal._motivePreview = true;
    if(this.proposal._descriptionPreview == undefined) this.proposal._descriptionPreview = true;

    this.proposal.files.map(async (file: any) => {
      file.url = await this.storage.getURL(file.url);
      this.files.push(file);
    })
  }

  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

}
