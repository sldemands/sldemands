import { Component, OnChanges, Input } from '@angular/core';
import { Photo } from '../../types/types';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnChanges {

  @Input()
  photo: Photo;
  url: string;
  fullscreen = false;
  constructor(
    private storage: StorageService
  ) { }

  ngOnChanges(): void {
    if(!this.photo.url) return;
    this.storage.getURL(this.photo.url).then(url => {
      this.url = url as string;
    })
  }
}
