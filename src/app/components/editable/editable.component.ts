import { Component, OnInit, OnChanges, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { DatastoreService } from '../../services/datastore.service';
import { EventService } from '../../services/event.service';
import { EditableStrings } from './strings'
import { convert } from 'html-to-text';

@Component({
  selector: 'app-editable',
  templateUrl: './editable.component.html',
  styleUrls: ['./editable.component.css']
})
export class EditableComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  data: any;
  @Input()
  path: string;
  @Input()
  previewLength: number;
  @Input()
  maxLength: number;
  @Input()
  html: boolean;
  @Input()
  editable = true;
  @Input()
  stripHTML = false;
  @Input()
  fullwidth = false;
  @Input()
  adminAllowed = true;

  @Input()
  previewCropped: boolean;
  @Output()
  previewCroppedChange = new EventEmitter<boolean>();

  @Input()
  preview: boolean;

  strings = EditableStrings;

  content = '';
  textEditable = false;
  hover = false;

  user: any;
  userSubscription: any;

  language = 0;
  languageSubscription: any;

  showEdit = false;
  showMt = false;

  constructor(
    private eventService: EventService,
    private datastore: DatastoreService
  ) {
    this.userSubscription = this.datastore.getUser().subscribe(user => {
      this.user = user;
      this.updateReactiveData();
    })
    this.languageSubscription = this.datastore.getLanguage().subscribe(language => {
      this.language = language;
      this.updateReactiveData();
    });
  }

  ngOnInit(): void {
    this.updateReactiveData();
  }

  ngOnChanges(): void {
    this.updateReactiveData();
  }

  updateReactiveData() {
    if(!this.data) return;
    if(this.data.placeholder) return;

    let propName = this.path.split(':')[1];

    let cropped = (convert(this.data[propName][this.language]).length > this.previewLength);
    this.previewCroppedChange.emit(cropped);

    if(cropped && this.preview) {
      this.content = convert(this.data[propName][this.language]).substring(0, this.previewLength) + '...';
    } else {
      this.content = this.data[propName][this.language];
      if(this.stripHTML) {
        this.content = convert(this.content);
      }
    }

    if(!this.content) this.content = '';

    this.content += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

    if(this.user && this.editable && !(cropped && this.preview)) {
      const isOwner = this.data.createdBy == this.user.uid;
      const isAdmin = this.user.admin;
      this.textEditable = isOwner || (isAdmin && this.adminAllowed);
    } else {
      this.textEditable = false;
    }

    if(this.data[propName + '_MT']) {
      this.showMt = this.data[propName + '_MT'][this.language];
    }
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.languageSubscription.unsubscribe();
  }

  edit() {
    let collection = this.path.split(':')[0];
    let propName = this.path.split(':')[1];
    this.eventService.emitEvent('show-dataedit', {
      data: this.data,
      language: this.language,
      html: this.html,
      maxLength: this.maxLength,
      collection: this.path.split(':')[0],
      propName: this.path.split(':')[1],
      callback: (modified: any) => {
        this.data[propName] = modified;
        this.updateReactiveData();
      }
    })
  }

  mt() {
    this.eventService.emitEvent('show-message', this.strings['mt'][this.language]);
  }
}
