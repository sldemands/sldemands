import { LanguageStrings } from '../../types/types';

export const EditableStrings: LanguageStrings = {
  mt: [
    'ස්වයංක්‍රිය පරිවර්තනයකි',
    'A machine translation',
    'தானியங்கி மொழிபெயர்ப்புு'
  ]
}
