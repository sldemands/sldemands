import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  loaderContent: string = '...&nbsp;&nbsp;&nbsp;';

  constructor() { }

  ngOnInit(): void {
    this.startProgress();
  }

  startProgress() {
    let blankIndex = 0;
    let direction = true;
    setInterval(() => {
      this.loaderContent = '';
      for(let i =0;i<blankIndex;i++) {
          this.loaderContent += '&nbsp;';
      }
      this.loaderContent += '...';
      for(let i =0;i<(3-blankIndex);i++) {
          this.loaderContent += '&nbsp;';
      }
      if(blankIndex == 3) {
        direction = false;
      }
      if(blankIndex == 0) {
        direction = true;
      }
      if(direction) blankIndex++;
      if(!direction) blankIndex--;
    }, 300);
  }

}
