import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';
import { DatabaseService } from '../../services/database.service';
import { DatastoreService } from '../../services/datastore.service';
import { UserRecord } from '../../types/types';
import { getAuth, getRedirectResult, FacebookAuthProvider } from "firebase/auth";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  showLogin: boolean = false;
  getNIC: boolean = false;
  name: string = '';
  nameError: boolean = false;
  email: string = '';
  emailError: boolean = false;
  nic: string = '';
  nicError: boolean = false;
  password: string = '';
  passwordError: boolean = false;
  repeatPassword: string = '';
  showPrivacyPolicy: boolean = false;
  loginStep: number = 0;
  signInUp: boolean = false;

  constructor(
    private eventService: EventService,
    private userService: UserService,
    private dbService: DatabaseService,
    private datastore: DatastoreService,
  ) { }

  ngOnInit(): void {
    this.eventService.registerEvent('show-hide-login').subscribe((value) => {
      if(!value) {
        this.closeAndResetLogin();
      } else {
        this.showLogin = true;
      }
    });

    // this.eventService.registerEvent('get-nic').subscribe(() => {
    //   this.getNIC = true;
    //   this.showLogin = true;
    // });
  }

  async createAccount() {
    let authUser = getAuth().currentUser;
    if(authUser) {
      this.datastore.setUser(await this.dbService.createUserRecord(
                                authUser.uid,
                                authUser.displayName? authUser.displayName: '',
                                authUser.email? authUser.email: '',
                                ''));
      this.closeAndResetLogin();
    }
  }

  hideSignin() {
    this.closeAndResetLogin();
  }

  validateEmail() {
    this.email = this.email.toLowerCase();
    if(!(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/).test(this.email)) {
      this.emailError = true;
      return false;
    }
    this.emailError = false;
    return true;
  }

  validateInputs() {
    if(this.name.length == 0) {
      this.nameError = true;
      return false;
    } else {
      this.nameError = false;
    }
    if(this.password.length < 7) {
      this.eventService.emitEvent('show-message', 'මුරපදය අකුරු 6ට වඩා වැඩි විය යුතුය');
      this.passwordError = true;
      return false;
    } else {
      this.passwordError = false;
    }
    if(this.signInUp && (this.repeatPassword != this.password)) {
      this.eventService.emitEvent('show-message', 'මුරපද නොගැලපේ');
      this.passwordError = true;
      return false;
    } else {
      this.passwordError = false;
    }
    return true;
  }

  async login() {
    if(this.loginStep == 0) {
      if(!this.validateEmail()) return;
      let userProvider = await this.dbService.checkUserExists(this.email);
      if(userProvider) {
        if(userProvider == 'facebook.com') {
          this.loginStep = -1;
        } else {
          this.signInUp = false;
          this.loginStep = 1;
        }
      } else {
        this.signInUp = true;
        this.loginStep = 1;
      }
      return;
    }

    if(this.loginStep = 1) {
      if(this.signInUp) {

        try {
          if(!this.validateInputs()) return;
          this.eventService.emitEvent('show-progress', true);
          let uid = await this.userService.signUpWithEmail(this.email, this.password);
          this.datastore.setUser(await this.dbService.createUserRecord(uid, this.name, this.email, this.nic));
          this.closeAndResetLogin();
        } catch (e) {
          const error = e as any;
          this.eventService.emitEvent('show-message', error.message);
        }

      } else {
        try {
          this.eventService.emitEvent('show-progress', true);
          let result = await this.userService.signInWithEmail(this.email, this.password);
          this.closeAndResetLogin();
        } catch (e) {
          const error = e as any;
          if(error) {
            this.eventService.emitEvent('show-message', error.message);
            this.passwordError = true;
          }
        }

      }
    }
  }

  closeAndResetLogin() {
    this.showLogin = false;
    this.loginStep = 0;
    this.signInUp = false;
    this.showPrivacyPolicy = false;
    this.password = '';
    this.repeatPassword = '';
    this.getNIC = false;
  }

  async facebookSignin() {
    await this.userService.facebookSigninRedirect();
  }
}
