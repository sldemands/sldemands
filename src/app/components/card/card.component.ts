import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  showBody: boolean = false;
  @Input() title: string = '';
  @Input() icon: string = '';
  @Input() description: string = '';
  @Input() route: string = '';
  @Input() link: string = '';
  @Input() expandTemplate: TemplateRef<any>;
  @Input() expanded: boolean;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.showBody = this.expanded;
  }

  goto() {
    if(this.link) {
      window.location.href = this.link;
    } else if(this.route) {
      this.router.navigate([this.route]);
    } else {
      this.showBody = !this.showBody;
    }
  }
}
