import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataeditComponent } from './dataedit.component';

describe('DataeditComponent', () => {
  let component: DataeditComponent;
  let fixture: ComponentFixture<DataeditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataeditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
