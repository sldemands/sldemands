import { Component, OnInit } from '@angular/core';
import { EditorConfig } from '../../configs/editor';
import { EventService } from '../../services/event.service';
import { HttpService } from '../../services/http.service';
import { DatastoreService } from '../../services/datastore.service';

@Component({
  selector: 'app-dataedit',
  templateUrl: './dataedit.component.html',
  styleUrls: ['./dataedit.component.css']
})
export class DataeditComponent implements OnInit {

  editorConfig = {
    ...EditorConfig,
    height: '200px',
    maxHeight: '200px'
  };

  showDataedit = false;
  newText = '';
  newHTML = '';
  error = false;
  event: any;
  mt = false;

  progress = false;

  constructor(
    private eventService: EventService,
    private httpService: HttpService,
    private datastore: DatastoreService
  ) {
    eventService.registerEvent('show-dataedit').subscribe((event: any) => {
      this.event = event;
      this.newText = event.data[event.propName][event.language];
      this.newHTML = event.data[event.propName][event.language];
      this.showDataedit = true;
    })
  }

  ngOnInit(): void {
  }

  hideDataedit() {
    this.showDataedit = false;
  }

  async submit() {
    if(this.progress) return;
    this.progress = true;

    let propData = JSON.parse(JSON.stringify(this.event.data[this.event.propName]));
    propData[this.event.language] = this.event.html? this.newHTML: this.newText;
    let propMTData = this.event.data[this.event.propName + '_MT'];
    propMTData[this.event.language] = this.mt;
    const response = await this.httpService.updateText({
      collection: this.event.collection,
      path: this.event.propName,
      id: this.event.data.id,
      data: propData,
      mt: propMTData,
      language: this.event.language
    }) as any;

    if(response.data.success) {
      this.event.callback(propData);
    } else if(response.data.accepted) {
      this.eventService.emitEvent('show-message', 'Update request sent to creator');
    }


    this.progress = false;
    this.showDataedit = false;

    // console.log(response);

  }
}
