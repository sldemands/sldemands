import { LanguageStrings } from '../../types/types';

export const ElectionStrings: LanguageStrings = {
  democraticTitle: [
    'යෝජනා',
    'Proposals',
    'வாக்களிப்பு'
  ],
  backButton: [
    'ආපසු',
    'Back',
    'திரும்பி வா'
  ],
  institutionName: [
    'ආයතනයේ නම',
    'Name of the institution',
    'நிறுவனத்தின் பெயர்',
  ],
  institutionDescription: [
    'ආයතනයේ විස්තරය',
    'About the organization',
    'அமைப்பு பற்றி'
  ],
  projectMotive: [
    'ව්‍යාපෘතියේ අරමුණ',
    'Purpose of the project',
    'திட்டத்தின் நோக்கம்'
  ],
  proposeButton: [
    'යෝජනාව එකතු කරන්න',
    'Add proposal',
    'முன்மொழிவைச் சேர்க்கவும்'
  ],
  proposeHeader: [
    'ඔබේ ආයතනය විසින් අරගලයට අදාළ පොදු ලේඛනයක් නිර්මාණය කර ඇත්නම්, ඔබට එය ජනතාවට බාගත කිරීමට සහ සාකච්ඡා කිරීම සඳහා මෙහි ඉදිරිපත් කළ හැක.',
    'If your institution has created a public document related to the aragalaya, you can submit it here for viewing and discussion.',
    'உங்கள் நிறுவனம் அரகலயா தொடர்பான பொது ஆவணத்தை உருவாக்கியிருந்தால், அதைப் பார்வையிடவும் கலந்துரையாடவும் இங்கே சமர்ப்பிக்கலாம்.'
  ],
  documentLabel: [
    'ලේඛනයක් එකතු කරන්න',
    'Add a document',
    'ஒரு ஆவணத்தைச் சேர்க்கவும்'
  ],
  urlLabel: [
    'යෝජනාවෙහි දිගුව :',
    'Proposal\'s url :',
    'முன்மொழிவு url :'
  ]
}
