import { Component, OnInit } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { Router } from '@angular/router';
import { DatastoreService } from '../../services/datastore.service';
import { DatabaseService } from '../../services/database.service';
import { EventService } from '../../services/event.service';
import { HttpService } from '../../services/http.service';
import { StorageService } from '../../services/storage.service';
import { ElectionStrings } from './strings';
import { EditorConfig } from '../../configs/editor';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-propose',
  templateUrl: './propose.component.html',
  styleUrls: ['./propose.component.css']
})
export class ProposeComponent implements OnInit {

  editorConfig = EditorConfig;

  language: number = 0;
  strings = ElectionStrings;

  institutionName = '';
  nameError: boolean = false;
  institutionDescription: string = '';
  descriptionError: boolean = false;
  projectMotive = '';
  motiveError = false;

  url: string = '';
  urlError: boolean = false;

  files: any[] = [];

  disableButton = false;
  disableFileButton = false;

  constructor(
    private dbService: DatabaseService,
    private router: Router,
    private eventService: EventService,
    private datastore: DatastoreService,
    private httpService: HttpService,
    private storage: StorageService
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
  }

  goBack() {
    this.router.navigate(['']);
  }

  urlChange() {
    this.url = this.url.replace(/[^a-zA-Z0-9-_]/g, '').toLowerCase();
  }

  async addFile(event: any) {
    if(this.disableFileButton) return;
    this.disableFileButton = true;

    let file = await this.storage.uploadToProposals(event.target.files[0] as File);
    this.files.push(file);

    this.disableFileButton = false;
  }

  async addProposal() {
    if(this.disableButton) return;
    this.disableButton = true;

    if(this.institutionName == '') {
      this.disableButton = false;
      this.nameError = true;
      return;
    }
    if(this.institutionDescription == '') {
      this.disableButton = false;
      this.descriptionError = true;
      return;
    }
    if(this.projectMotive == '') {
      this.disableButton = false;
      this.motiveError = true;
      return;
    }

    let auth = getAuth();
    if(!auth.currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
      this.disableButton = false;
      return;
    } else {
      let user = await firstValueFrom(this.datastore.getUser());
      await this.httpService.createProposal({
        name: this.institutionName,
        description: this.institutionDescription,
        purpose: this.projectMotive,
        files: this.files,
        verified: user.verified,
        createdOn: (new Date()).getTime(),
        createdBy: auth.currentUser.uid
      });
      this.router.navigate([''])
    }
    this.disableButton = false;
  }
}
