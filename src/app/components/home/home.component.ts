import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BlankBulletin } from '../../components/bulletin/bulletin.component'
import { DatastoreService } from '../../services/datastore.service';
import { DatabaseService } from '../../services/database.service';
import { HttpService } from '../../services/http.service';
import { StorageService } from '../../services/storage.service';
import { EventService } from '../../services/event.service';
import { IconStrings } from './strings';
import { convert } from 'html-to-text';
import { getAuth, signOut } from "firebase/auth";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  strings = IconStrings;
  language: number = 0;
  loggedIn = false;
  elections: any[];

  bulletins: any[] = [BlankBulletin, BlankBulletin];
  news: any = {
    createdOn: new Date().getTime(),
    code: '',
    placeholder: true
  };
  discussion: any = {
    createdOn: new Date().getTime(),
    code: '',
    placeholder: true
  };
  photo: any = {
    url: '',
    placeholder: true
  }
  proposals: any = [{
    name: ['','','',''],
    description: ['','','',''],
    purpose: ['','','',''],
    placeholder: true
  }]

  code = `<iframe width="560" height="315" src="https://www.youtube.com/embed/a6GYVSdfyH4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
  time = 1652266248103;

  constructor(
    private datastore: DatastoreService,
    public router: Router,
    private httpService: HttpService,
    private dbService: DatabaseService,
    private storage: StorageService,
    private eventService: EventService
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
    datastore.getUser().subscribe(user => (this.loggedIn = !!user));
    this.dbService.getElections().then((elections: any) => {
      this.elections = elections.map((e: any) => {
        return {
          ...e,
          descriptionText: e.description.map((d: any) => convert(d))
        }
      });
    });
    this.dbService.getTopBulletins(2).then(bulletins => (this.bulletins = bulletins));
    this.dbService.getTopNews().then(news => (this.news = news));
    this.dbService.getTopDiscussion().then(discussion => (this.discussion = discussion));
    this.dbService.getTopPhoto().then(photo => (this.photo = photo));
    this.dbService.getProposals().then(pro => {this.proposals = pro});
  }

  ngOnInit(): void {}

  gotoLink(url: string) {
    window.location.href = url;
  }

  goto(path: string) {
    this.router.navigate([path]);
  }

  login() {
    if(!this.loggedIn) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      signOut(getAuth()).then(() => {
        this.loggedIn = false;
        this.datastore.setUser(undefined);
      });
    }
  }
}
