import { LanguageStrings } from '../../types/types';

export const IconStrings: LanguageStrings = {
  proposalsTitle: [
    'මහජන අදහස් සඳහා විවෘත යෝජනා',
    'Proposals for public comments',
    'பொது கருத்துகளுக்கான முன்மொழிவுகள்'
  ],
  citizenForumTitle: [
    'පුරවැසි සභාව',
    'Citizen Forum',
    'குடிமக்கள் மன்றம்'
  ],
  citizenForumDescription: [
    'රට ගොඩ නැගීමට ඇති ඔබගේ යෝජනා ඇතුලත් කරන්න මෙම යොමුවට පිවිසෙන්න >',
    'Click here to enter and view your ideas for developing the nation >',
    'தேசத்தை வளர்ப்பதற்கான உங்கள் யோசனைகளை உள்ளிடவும் பார்க்கவும் இங்கே கிளிக் செய்யவும் >'
  ],
  democraticTitle: [
    'අරගල මත විමසීම්',
    'Polls',
    'கருத்துக் கணிப்பு'
  ],
  democraticDescription: [
    'අරගලයේ ඉල්ලීම් සහ නායකයන්ට ඡන්දේ දෙන්න.',
    'Vote for demands and leaders of the struggle',
    'கோரிக்கைகளுக்கும் போராட்டத் தலைவர்களுக்கும் வாக்களியுங்கள்'
  ],
  newsTitle: [
    'අරගලය පුවත්',
    'Aragalaya News',
    'புரட்சி செய்தி'
  ],
  newsDescription: [
    'අරගල භුමියේ සිට ගෙනෙන පුවත්.',
    'News from the protest sites.',
    'போராட்டத் தளங்களில் இருந்து வரும் செய்திகள்.'
  ],
  discussionsTitle: [
    'අරගලය සාකච්ඡා',
    'Aragalaya Discussions',
    'புரட்சி விவாதங்கள்'
  ],
  discussionsDescription: [
    'අරගලය පිලිබඳ පුරවැසි සංවාද.',
    'Citizen conversations about the struggle.',
    'போராட்டம் பற்றிய குடிமக்கள் உரையாடல்.'
  ],
  bulletinTitle: [
    'දැන්වීම් පුවරුව',
    'Bulletin Board',
    'புரட்சி விவாதங்கள்'
  ],
  bulletinDescription: [
    'අරගලය පිලිබඳ ලුහුඬු දැන්වීම් කියවන්න හා ඇතුල් කරන්න.',
    'A bulletin board for the people\'s struggle.',
    'மக்கள் போராட்டத்திற்கான அறிவிப்பு பலகை.'
  ],
  constitutionTitle: [
    'පුරවැසි මූලික ව්‍යවස්ථාව',
    'People Centered Constitution',
    'மக்களை மையப்படுத்திய அரசியலமைப்பு'
  ],
  galleryTitle: [
    'ගැලරිය',
    'Gallery',
    'கேலரி'
  ],
  petitionsTitle: [
    'පෙත්සම්',
    'Petitions',
    'மனுக்கள்'
  ],
  aboutTitle: [
    'අපි ගැන',
    'About us',
    'எங்களை பற்றி'
  ],
  contributeTitle: [
    'දායක වෙන්න',
    'Contribute',
    'பங்களிக்க'
  ],
  electionsHeader: [
    'මත විමසීම්',
    'Polls',
    'வாக்களிப்ப'
  ],
  votesLabel: [
    'ප්‍රකාශිත චන්ද',
    'Casted votes',
    'வாக்குகளை அளித்தனர்'
  ],
  nominations: [
    'යෝජනා :',
    'Nomination :',
    'நியமனம் :'
  ],
  casted: [
    'මනාප :',
    'Votes :',
    'வாக்குகள் :'
  ],
  remaining: [
    'ඉතුරු මනාප :',
    'Remaining :',
    'மீதமுள்ள :'
  ],
  moreBulletins: [
    'තවත් දැන්වීම්',
    'More bulletins',
    'மேலும் புல்லட்டின்கள்'
  ],
  moreNews: [
    'තවත් පුවත්',
    'More news',
    'மேலும் செய்திகள்'
  ],
  moreDiscussions: [
    'තවත් සාකච්ඡා',
    'More discussions',
    'மேலும் விவாதங்கள்'
  ],
  callElection: [
    'මත විමසීම්ක් කැඳවන්න',
    'Create a poll',
    'கருத்துக்கணிப்பை கோரவும்'
  ],
  morePhotos: [
    'තවත් පින්තූර..',
    'More photos..',
    'மேலும் புகைப்படங்கள்்..'
  ],
  aboutUs: [
    `aragalaya.online යනු, ශ්‍රී ලංකාවට 'ක්‍රමයේ වෙනසක්' ඉල්ලා සටන් වදින, දේශය පුරා විසිරී සිටින විවිධ පුද්ගයලයන් හා විවිධ සංවිධානයන්ගේ එකතුවෙන් බල ගැන්වෙන සමස්ථ පුරවැසි අරගයේ ඩිජිටල් සම්ප්‍රප්තියයි.
    GoHomeGota හා #GotaGoGama යන හෑෂ්ටැග් සලකුණු යටතේ මෙතෙක් ශ්‍රී ලංකාව තුළ ඇති වූ මෙම විශාලතම සාමකාමී පුරවැසි අරගලය, දෙස් විදෙස් හි වෙසෙන සියලූ ශ්‍රී ලාංකිකයන්ගෙන් සන්නද්ධයි.<br><br>
    මේ තුළින් සමස්ථ ශ්‍රීලාංකිකයන් බලාපොරොත්තු වන්නේ වටිනාකම්වලින් පිරි දේශපාලන, ආර්ථික හා සමාජීය වශයෙන් ක්‍රමයේ වෙනසක් හරහා ශ්‍රී ලංකාව නව මාවතකට යොමු කරමින් තිරසර සංවර්ධනයක් අත්පත් කරගැනීමයි. විනිවිදභාවය, සමානාත්මතාවය හා නිමාණශීලීත්වය පෙරටු, ප්‍රචාන්ඩත්වයෙන් තොර ව කෙරෙන සාමකාමී අරගලයක වටිනාකම් සුරුකීමට අප නිරතුරුවම කැප වෙනවා.<br><br> මෙම සාමකාමී පුරවැසි අරගලයේ පොදු අරමුණු සාක්ෂාත් කරගැනීම වෙනුවෙන් අවංකව පෙළ ගැසෙන ඕනෑම පාර්ශවයකට aragalaya.online වෙබ් අඩවිය විවෘත වේදිකාවක් වනු නො අනුමානයි. ගේල්ෆේස් උද්ඝෝෂණ පිටිය (රජය විසින් ස්ථාපිත කළ) මූලික කරගෙන දේශය පුරා ඇති #GotaGoGama සමාජ සමූහයන් විසින් සිදු කෙරෙන සාමකාමී පුරවැසි අරගලයේ විෂය මූලික වන සත්‍ය තොරතුරු නිරන්තරයෙන් සමාජ ගත කිරීම මෙම වෙබ් අඩවියේ මූලික මෙහෙවර වේ. <br><br>
    Source repository:<br>
    <a href="https://gitlab.com/sldemands/sldemands">https://gitlab.com/sldemands/sldemands</a>`,
    `Aragalaya.online is the digital footprint of the largest collective of organizations and individuals who are fighting for the most essential system change in Sri Lanka.<br><br> Under the hashtags of #GoHomeGota and #GotaGoGama, Sri Lanka’s largest ever people’s protest campaign, actively seeks a value based new political, economic and social systems that will place Sri Lanka on a path to inclusive, just and sustainable development for one and all. We are driven and bound by our shared values such as (but not limited to) transparency, inclusiveness, creativity and non-violence.<br><br> Aragalaya.online is open to collaboration with all interested parties who are genuinely committed to achieve shared goals of all citizens who acknowledge and support the ongoing people’s revolution. This site’s mission is to provide reliable, true and objective information about the people’s movement which symbolically occupies the state declared agitation site at Gallface, Colombo and the multiple #gotagogama communities around the country.<br><br>
    Source repository:<br>
    <a href="https://gitlab.com/sldemands/sldemands">https://gitlab.com/sldemands/sldemands</a>`,
    `இலங்கையில் நிர்வாகக் கட்டமைப்பில் மாற்றத்தை ஏற்படுத்தக் கோரி போராடும் பெருமளவான அமைப்புகள் மற்றும் தனிநபர்களின் திரண்ட டிஜிட்டல் பிரசன்னமாக Aragalaya.online அமைந்துள்ளது. அனைவருக்கும் நிலைபேறான அபிவிருத்தியை நோக்கி கொண்டு செல்லக்கூடிய உள்ளடக்கமான வகையில் அமைந்திருக்கும், பெறுமதி அடிப்படையிலான புதிய அரசியல், பொருளாதார மற்றும் சமூக கட்டமைப்புகளை கோரி இலங்கையின் மாபெரும் மக்கள் போராட்டம் #GoHomeGota மற்றும் #GotaGoGama ஆகிய hashtagகளின் கீழ் முன்னெடுக்கப்படுகின்றன. வெளிப்படைத்தன்மை, உள்ளடக்கம், ஆக்கத்திறன் மற்றும் வன்முறை இன்மை (ஆகியவற்றுக்கு மட்டுப்படுத்தப்படாத) போன்ற பகிரப்பட்ட பெறுமதிகளின் அடிப்படையில் நாம் வழிநடத்தப்படுகின்றோம். சகல குடிமக்களின் பகிரப்பட்ட பெறுமதிகளை எய்துவதற்கு உண்மையில் தம்மை அர்ப்பணித்து, தற்போது முன்னெடுக்கப்படும் மக்கள் புரட்சிக்கு ஆதரவளிக்கக்கூடிய, ஆர்வமுள்ள சகல தரப்பினருக்கும் கைகோர்க்கக்கூடியதாக Aragalaya.online அமைந்துள்ளது. அரசாங்கத்தினால் கொழும்பு, காலி முகத்திடல் பகுதியில் போராட்டங்களை முன்னெடுப்பதற்காக ஒதுக்கப்பட்டுள்ள பகுதியில் அடையாளமாக அமையப் பெற்றுள்ள மற்றும் நாடு முழுவதிலும் வெவ்வேறு சமூகத்தினரால் முன்னெடுக்கப்படும் பல்வேறு #gotagogama போராட்டங்களிலும் முன்னெடுக்கப்படும் மக்களின் நடவடிக்கைகள் தொடர்பில் உண்மையான, நம்பத்தகுந்த மற்றும் நோக்குடைய தகவல்களை பெற்றுக் கொடுப்பது இந்த தளத்தின் நோக்காக அமைந்துள்ளது.<br><br>
    Source repository:<br>
    <a href="https://gitlab.com/sldemands/sldemands">https://gitlab.com/sldemands/sldemands</a>`
  ]
}
