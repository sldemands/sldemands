import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PollStrings } from './strings'
import { DatastoreService } from '../../services/datastore.service';
import { DatabaseService } from '../../services/database.service';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit, OnDestroy {

  @Input()
  poll: any;
  strings = PollStrings;
  language: number = 0;
  languageSubscription: any;
  minimized = true;
  nominationCount: string|number = '-';

  constructor(
    private datastore: DatastoreService,
    private dbService: DatabaseService,
    public router: Router
  ) {
    this.languageSubscription = datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
    this.dbService.getItems(this.poll.id).then(n => (this.nominationCount = n.length))
  }

  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

  goto(path: string) {
    this.router.navigate([path]);
  }
}
