import { LanguageStrings } from '../../types/types';

export const PollStrings: LanguageStrings = {
  electionsHeader: [
    'මත විමසීම්',
    'Polls',
    'வாக்களிப்ப'
  ],
  votesLabel: [
    'ප්‍රකාශිත ඡන්ද',
    'Casted votes',
    'வாக்குகளை அளித்தனர்'
  ],
  nominations: [
    'යෝජනා :',
    'Nomination :',
    'நியமனம் :'
  ],
  casted: [
    'මනාප :',
    'Votes :',
    'வாக்குகள்'
  ],
  remaining: [
    'ඉතුරු මනාප :',
    'Remaining :',
    'மீதமுள்ள :'
  ],
  vote: [
    'ඡන්දය දෙන්න',
    'vote',
    'வாக்கு'
  ]
}
