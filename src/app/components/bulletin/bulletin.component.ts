import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Bulletin } from '../../types/types';
import { convert } from 'html-to-text'
import { DatastoreService } from '../../services/datastore.service';
import { Router } from '@angular/router'

export const BlankBulletin = {
  verified: false,
  title: ['-','-','-','-'],
  description: ['-','-','-','-'],
  createdOn: 0,
  id: '',
  placeholder: true
}

@Component({
  selector: 'app-bulletin',
  templateUrl: './bulletin.component.html',
  styleUrls: ['./bulletin.component.css']
})
export class BulletinComponent implements OnChanges {

  @Input()
  bulletin: Bulletin;

  language: number = 0;
  descriptionText: string[];

  date: string;
  monthString: string = '-';
  timeString: string = '---';
  minimized: boolean = true;

  constructor(
    private datastore: DatastoreService,
    public router: Router
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['bulletin'].currentValue) {
      this.descriptionText = (this.bulletin.description as string[]).map(d => convert(d));

      let date = new Date(this.bulletin.createdOn);
      if(!this.bulletin.placeholder) {
        this.monthString = date.toLocaleString('default', { month: 'short' }) + ' ' + date.getDate();
        this.timeString = date.toTimeString().substring(0,9);
      }
    }
  }

  expandClick() {
    this.minimized = !this.minimized;
  }

}
