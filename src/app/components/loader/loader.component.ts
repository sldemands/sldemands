import { Component, OnChanges, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit, OnChanges {

  @Input()
  width = '10';
  @Input()
  loaded = false;
  text = '';

  constructor() { }

  ngOnInit(): void {
    for(let i=0;i<parseInt(this.width);i++) this.text+='-';
  }

  ngOnChanges(): void {
    this.text = '';
    for(let i=0;i<parseInt(this.width);i++) this.text+='-';
  }

}
