import { Component, OnInit } from '@angular/core';
import { DatastoreService } from '../../services/datastore.service';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {

  language: number = 0;

  constructor(
    private datastore: DatastoreService
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
  }

  selectLanguage(val: number) {
    this.datastore.setLanguage(val);
  }
}
