import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { DatastoreService } from '../../services/datastore.service';
import { HttpService } from '../../services/http.service';
import { UserService } from '../../services/user.service';
import { EventService } from '../../services/event.service';
import { DatabaseService } from '../../services/database.service';
import { CommentStrings } from './strings';
import { firstValueFrom } from 'rxjs';

const MONTH_NAMES = [
  'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December'
];

const VERIFIED_COLLECTIONS = [
  'Bulletin', 'Bulletin-dev', 'News', 'News-dev', 'Discussions',
  'Discussion-dev', 'Proposals', 'Proposals-dev'
];

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit, OnChanges, OnDestroy {

  _model: any = {};
  @Input('model')
  set model(value: any) {
    this._model = value;
  }
  get model() {
    return this._model;
  }

  @Input()
  collection: string;

  hideVerified = true;;
  commentsCount = '';
  comment = '';
  commentError = false;
  timeAgoString = '';
  strings = CommentStrings;
  disableButton = false;
  userColor: string = ''
  progress = false;

  language = 0;
  languageSubscription: any;
  comments: any[]|undefined = undefined;
  commentsSubscription: any;

  constructor(
    private datastore: DatastoreService,
    private httpService: HttpService,
    private userService: UserService,
    private dbService: DatabaseService,
    private eventService: EventService
  ) {
    this.languageSubscription = datastore.getLanguage().subscribe(val => {
      this.language = val;
      this.updateReactiveData();
    });
  }

  ngOnInit(): void {
    this.hideVerified = !VERIFIED_COLLECTIONS.includes(this.collection);
    this.updateReactiveData();
  }

  ngOnChanges(): void {
    this.updateReactiveData();
  }

  updateReactiveData() {
    if(this.model && this.model.id && (this.commentsSubscription == undefined)) {
      this.commentsSubscription = this.dbService.getCommentsListener(this.model.id).subscribe(c => {
        this.comments = c;
      })
    }

    this.timeAgoString = this.timeAgo(this.model.createdOn);

    this.commentsCount = this.strings['comments'][this.language]
      .replace('*count*', this.comments? this.comments.length.toString(): '0')
  }

  ngOnDestroy() {
    this.languageSubscription.unsubscribe();
    if(this.commentsSubscription) {
      this.commentsSubscription.unsubscribe();
    }
  }

  async submitComment() {
    if(this.disableButton) return;
    this.disableButton = true;

    if(!this.comment) {
      this.commentError = true;
      this.disableButton = false;
      return;
    }
    if(!getAuth().currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      this.progress = true;
      const color = [0,0,0];
      let user = await firstValueFrom(this.datastore.getUser());
      if(user) {
        const hash = djb2(user.uid);
        color[0] = (hash & 0xFF0000) >> 16;
        color[1] = (hash & 0x00FF00) >> 8;
        color[2] = (hash & 0x0000FF);

        const comment = {
          name: user.name,
          content: this.comment,
          createdOn: (new Date()).getTime(),
          createdBy: user.uid,
          color: color
        }
        try {
          await this.httpService.createComment({
            ...comment,
            collection: this.collection,
            id: this.model.id
          });
          // fetch the translated comment from the db
          // this.comments = await this.dbService.getComments(this.model.id);
          this.comment = '';
          this.progress = false;
        } catch (error) {
          console.log(error);

          this.dbService.createError(error as any, 'comments:submitComment');
          this.progress = false;
        }
      }
    }

    this.disableButton = false;
  }

  arrayToRGB(color: number[]) {
    return 'rgb(' + color[0] + ',' + color[1] + ',' + color[2] + ')';
  }

  mt() {
    this.eventService.emitEvent('show-message', this.strings['mt'][this.language]);
  }

  timeAgo(dateParam: Date) {
    if (!dateParam) {
      return '';
    }
    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(today.getTime() - DAY_IN_MS);
    const seconds = Math.round((today.getTime() - date.getTime()) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();

    if (seconds < 5) {
      return this.strings['now'][this.language];
    } else if (seconds < 60) {
      return this.strings['secondsAgo'][this.language].replace('*seconds*', seconds.toString());
    } else if (seconds < 90) {
      return this.strings['minuteAgo'][this.language];
    } else if (minutes < 60) {
      return this.strings['minutesAgo'][this.language].replace('*minutes*', minutes.toString());
    } else if (isToday) {
      return this.getFormattedDate(date, this.strings['today'][this.language]);
    } else if (isYesterday) {
      return this.getFormattedDate(date, this.strings['yesterday'][this.language]);
    } else if (isThisYear) {
      return this.getFormattedDate(date, false, true);
    }
    return this.getFormattedDate(date);
  }

  getFormattedDate(date: Date, prefomattedDate: any = false, hideYear = false) {
    const day = date.getDate();
    const month = MONTH_NAMES[date.getMonth()];
    const year = date.getFullYear();
    const hours = date.getHours();
    let minutes: any = date.getMinutes();
    if (minutes < 10) {
      minutes = `0${ minutes }`;
    }
    if (prefomattedDate) {
      return `${ prefomattedDate } ${(this.language == 1)? 'at': ''} ${ hours }:${ minutes } ${(this.language == 0)? 'ට': ((this.language==2)? 'மணிக்கு': '')}`;
    }
    if (hideYear) {
      return `${ day }. ${ month } ${(this.language == 1)? 'at': ''} ${ hours }:${ minutes } ${(this.language == 0)? 'ට': ((this.language==2)? 'மணிக்கு': '')}`;
    }
    return `${ day }. ${ month } ${ year }. ${(this.language == 1)? 'at': ''} ${ hours }:${ minutes }`;
  }
}

function djb2(str: string){
  var hash = 5381;
  for (var i = 0; i < str.length; i++) {
    hash = ((hash << 5) + hash) + str.charCodeAt(i); /* hash * 33 + c */
  }
  return hash;
}

function hashStringToColor(str: string) {
  var hash = djb2(str);
  var r = (hash & 0xFF0000) >> 16;
  var g = (hash & 0x00FF00) >> 8;
  var b = hash & 0x0000FF;
  return "#" + ("0" + r.toString(16)).substr(-2) + ("0" + g.toString(16)).substr(-2) + ("0" + b.toString(16)).substr(-2);
}
