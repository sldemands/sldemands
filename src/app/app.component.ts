import { Component, OnInit, OnDestroy, isDevMode, ElementRef, Inject } from '@angular/core';
import { getFunctions, httpsCallable } from 'firebase/functions';
import { getStorage } from "firebase/storage";
import { UserService } from './services/user.service';
import { EventService } from './services/event.service';
import { DatabaseService } from './services/database.service';
import { DatastoreService } from './services/datastore.service';
import { StorageService } from './services/storage.service';
import { HttpService } from './services/http.service';
import { initializeApp } from "firebase/app";
import { getFirestore, collection, query, where, getDocs } from "firebase/firestore";
import { getAuth, setPersistence, browserLocalPersistence, onAuthStateChanged } from "firebase/auth";
import { UserRecord } from './types/types';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  showProgress: boolean = false;
  showMessage: boolean = false;
  message: string = '';
  mode: number = 0;
  language: number = 0;
  languageSubscription: any;

  constructor(
    private userService: UserService,
    private eventService: EventService,
    private dbService: DatabaseService,
    private datastore: DatastoreService,
    private httpService: HttpService,
    private storage: StorageService,
    @Inject(DOCUMENT) private documentRef: any
  ) {
    // Firebase config
    const firebaseConfig = {
      apiKey: "AIzaSyDbZAkoGqDxIEB4TPkfTB7yh8_nFHGbhvM",
      authDomain: "aragalaya.online",
      projectId: "sldemands",
      storageBucket: "sldemands.appspot.com",
      messagingSenderId: "789008593458",
      appId: "1:789008593458:web:08067baa6bdffd07edc22b",
      measurementId: "G-2WSNYHTMJJ",
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    httpService.http = getFunctions(app);
    dbService.db = getFirestore(app);
    storage.storage = getStorage(app);
    const auth = getAuth();
    setPersistence(auth, browserLocalPersistence);

    onAuthStateChanged(auth, async (user) => {
      if(user) {
        const idTokenResult = await user.getIdTokenResult();
        const admin = !!idTokenResult.claims['admin'];

        let userRecord = await dbService.getUserRecord(user.uid) as UserRecord;
        if(userRecord) {
          if(admin == !!userRecord.admin) {
            datastore.setUser(userRecord);
          } else {
            let updated = await dbService.updateUserRecord({
              admin: true
            });
            if(updated) {
              userRecord.admin = true;
              datastore.setUser(userRecord);
            }
          }
        } else {
          if(user.providerId != 'firebase') {
            this.datastore.setUser(
              await dbService.createUserRecord(user.uid, user.displayName as string, user.email as string, '')
            )
          }
        }
        eventService.emitEvent('show-progress', false);
      } else {
        this.datastore.setUser(undefined);
      }
    });

    this.languageSubscription = datastore.getLanguage().subscribe(val => {
      documentRef.all[0].className = (val == 1) ? 'english' : ((val == 2) ? 'tamil' : '');
      this.language = val;
    });
  }

  ngOnInit() {
    if(!isDevMode()) {
      this.dbService.createLog('page-load');
    }

    this.eventService.registerEvent('show-progress').subscribe(value => {
      this.showProgress = value;
      this.showMessage = false;
    });

    this.eventService.registerEvent('show-message').subscribe(value => {
      this.message = value;
      this.showProgress = false;
      this.showMessage = true;
      setTimeout(() => {
        this.showMessage = false;
      }, 4000);
    });
  };

  ngOnDestroy() {
    this.languageSubscription.unsubscribe();
  }

  modeChanged(val: number) {
    this.mode = val;
  }

}
