import { LanguageStrings } from '../../types/types';

export const GalleryStrings: LanguageStrings = {
  galleryTitle: [
    'ගැලරිය',
    'Gallery',
    'கேலரி'
  ],
  addPhoto: [
    'එකතු කරන්න',
    'Add photo',
    'புகைப்படம் சேர்க்க'
  ],
  morePhotos: [
    'තවත් පින්තූර..',
    'More photos..',
    'மேலும் புகைப்படங்கள்்..'
  ],
  backButton: [
    'ආපසු',
    'Back',
    'திரும்பி வா'
  ]
}
