import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from '../../services/database.service';
import { DatastoreService } from '../../services/datastore.service';
import { StorageService } from '../../services/storage.service';
import { EventService } from '../../services/event.service';
import { Photo } from '../../types/types';
import { GalleryStrings } from './strings';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  @ViewChild('uploader') upload: ElementRef<any>;
  language: number = 0;
  path: string = '';
  photos: Photo[] = [];
  morePhotos: Photo[] = [];
  strings = GalleryStrings;

  showAdd: boolean = false;
  showLoader: boolean = true;
  loaderContent: string = '...';

  disableButton: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbService: DatabaseService,
    private datastore: DatastoreService,
    private eventService: EventService,
    private storage: StorageService
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
    this.getPhotos();
  }

  getPhotos() {
    this.dbService.getPhotos().then(async (photos) =>  {
      let photosTemp = photos.sort((a: any, b: any) => (b.createdOn-a.createdOn));
      for(let [index, photo] of photosTemp.entries()) {
        if(index < 4) {
          this.photos.push(photo);
          await new Promise<void>((resolve) => (setTimeout(() => resolve(), 100)));
        } else {
          this.morePhotos.push(photo);
        }
      }
    });
    setTimeout(() => {
      this.showLoader = false;
    }, 3000);
  }

  goBack() {
    this.router.navigate(['']);
  }

  selectLanguage(val: number) {
    this.datastore.setLanguage(val);
  }

  loadMorePhotos() {
    let lastIndex = 0;
    this.morePhotos.map((video, index) => {
      if(index < 4) {
        lastIndex = index;
        this.photos.push(video);
      }
    });
    this.morePhotos.splice(0, lastIndex + 1);
  }

  async addPhoto(event: any) {
    if(this.disableButton) return;
    this.disableButton = true;

    await this.storage.uploadToGallery(event.target.files[0] as File);
    this.getPhotos();

    this.disableButton = false;
  }

  addItem() {
    if(this.disableButton) return;
    this.disableButton = true;

    if(!getAuth().currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      this.upload.nativeElement.click();
    }

    this.disableButton = false;
  }
}
