import { LanguageStrings } from '../../types/types';

export const MediaStrings: LanguageStrings = {
  newsTitle: [
    'අරගලය පුවත්',
    'Aragalaya News',
    'புரட்சி செய்தி'
  ],
  discussionsTitle: [
    'අරගලය සාකච්ඡා',
    'Aragalaya Discussions',
    'புரட்சி விவாதங்கள்'
  ],
  reportButton: [
    'වාර්තා කරන්න',
    'Add video',
    'வீடியோவைச் சேர்க்கவும்'
  ],
  moreVideos: [
    'තවත් වීඩියෝ..',
    'More videos..',
    'மேலும் வீடியோக்கள்..'
  ],
  backButton: [
    'ආපසු',
    'Back',
    'திரும்பி வா'
  ]
}
