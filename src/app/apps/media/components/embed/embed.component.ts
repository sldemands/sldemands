import { Component, OnInit } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { DatabaseService } from '../../../../services/database.service';
import { DatastoreService } from '../../../../services/datastore.service';
import { UserService } from '../../../../services/user.service';
import { EventService } from '../../../../services/event.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-embed',
  templateUrl: './embed.component.html',
  styleUrls: ['./embed.component.css']
})
export class EmbedComponent implements OnInit {

  error: boolean = false;
  code: string = '';
  path: string = '';

  disableButton: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbService: DatabaseService,
    private eventService: EventService,
    private userService: UserService,
    private datastore: DatastoreService
  ) { }

  ngOnInit(): void {
    this.route.url.subscribe(val => (this.path = val[0].path));
  }

  async embedVideo() {

    if(!this.code.includes('iframe')) {
      this.error = true;
      return;
    }
    this.error = false;

    if(this.disableButton) return;
    this.disableButton = true;

    let auth = getAuth();

    if(!auth.currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      let user = await firstValueFrom(this.datastore.getUser());
      if(user) {
        let item = {
          code: this.code,
          createdBy: user.uid,
          createdOn: new Date().getTime(),
          verified: user.verified
        };
        if(this.path == 'news') {
          await this.dbService.embedNewsVideo(item);
          this.router.navigate(['news']);
        }
        if(this.path == 'discussions') {
          await this.dbService.embedDiscussionVideo(item);
          this.router.navigate(['discussions']);
        }
      }
    }
    this.disableButton = false;
  }
}
