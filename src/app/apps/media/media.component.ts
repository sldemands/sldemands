import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from '../../services/database.service';
import { DatastoreService } from '../../services/datastore.service';
import { EventService } from '../../services/event.service';
import { Video } from '../../types/types';
import { MediaStrings } from './strings';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  language: number = 0;
  path: string = '';
  videos: Video[] = [];
  moreVideos: Video[] = [];
  strings = MediaStrings;

  showLoader: boolean = true;
  loaderContent: string = '...';

  disableButton: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbService: DatabaseService,
    private datastore: DatastoreService,
    private eventService: EventService
  ) {
    let blankIndex = 0;
    let direction = true;
    setInterval(() => {
      this.loaderContent = '';
      for(let i =0;i<blankIndex;i++) {
          this.loaderContent += '&nbsp;';
      }
      this.loaderContent += '...';
      if(blankIndex == 4) {
        direction = false;
      }
      if(blankIndex == 0) {
        direction = true;
      }
      if(direction) blankIndex++;
      if(!direction) blankIndex--;
    }, 500);
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {

    this.route.url.subscribe(val => {
      this.path = val[0].path;
      if(this.path == 'news') {
        this.dbService.getNewsVideos().then(async (videos) =>  {
          let videosTemp = videos.sort((a,b) => (b.createdOn-a.createdOn));
          for(let [index, video] of videosTemp.entries()) {
            if(index < 4) {
              this.videos.push(video);
              await new Promise<void>((resolve) => (setTimeout(() => resolve(), 100)));
            } else {
              this.moreVideos.push(video);
            }
          }
        });
      }
      if(this.path == 'discussions') {
        this.dbService.getDiscussionVideos().then(async (videos) =>  {
          let videosTemp = videos.sort((a,b) => (b.createdOn-a.createdOn));
          for(let [index, video] of videosTemp.entries()) {
            if(index < 4) {
              this.videos.push(video);
              await new Promise<void>((resolve) => (setTimeout(() => resolve(), 100)));
            } else {
              this.moreVideos.push(video);
            }
          }
        });
      }
    });
    setTimeout(() => {
      this.showLoader = false;
    }, 3000);
  }

  goBack() {
    this.router.navigate(['']);
  }

  selectLanguage(val: number) {
    this.datastore.setLanguage(val);
  }

  loadMoreVideos() {
    let lastIndex = 0;
    this.moreVideos.map((video, index) => {
      if(index < 4) {
        lastIndex = index;
        this.videos.push(video);
      }
    });
    this.moreVideos.splice(0, lastIndex + 1);
  }

  addItem() {
    if(this.disableButton) return;
    this.disableButton = true;

    if(!getAuth().currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      if(this.path == 'news') {
        this.router.navigate(['news', 'embed']);
      } else {
        this.router.navigate(['discussions', 'embed']);
      }
    }

    this.disableButton = false;
  }
}
