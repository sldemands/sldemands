import { LanguageStrings } from '../../types/types';

export const ElectionStrings: LanguageStrings = {
  democraticTitle: [
    'ඡන්ද විමසීම්',
    'Polls',
    'வாக்களிப்பு'
  ],
  backButton: [
    'ආපසු',
    'Back',
    'திரும்பி வா'
  ],
  electionTitle: [
    'මැතිවරණයේ නම',
    'Poll title',
    'தேர்தல் பெயர்'
  ],
  electionDescription: [
    'මැතිවරණය කැඳවීමට හේතු සහ විස්තරය',
    'Reasons for holding the election',
    'தேர்தல் நடத்துவதற்கான காரணங்கள்'
  ],
  nominationLabel: [
    'උපරිම නාමයෝජනා ගණන',
    'Maximum number of nominations',
    'பரிந்துரைகளின் அதிகபட்ச எண்ணிக்கை'
  ],
  votesLabel: [
    'එක් පුද්ගලයෙකුට හිමි චන්ද ගණන',
    'Maximum number of allowed votes per person',
    'ஒரு நபருக்கு அனுமதிக்கப்பட்ட அதிகபட்ச வாக்குகள்'
  ],
  electionButton: [
    'මැතිවරණය කැඳවන්න',
    'Call the election',
    'தேர்தலுக்கு அழைப்பு விடுங்கள்'
  ],
  electionHeader: [
    'අරගලයට සම්බන්ද ඕනෑම කරුණක් පිළිබඳව අරගල කරුවන්ගේ මතය විමසීම සඳහා ඔබට මැතිවරණයක් කැඳවිය හැක.',
    'Call an election to find the protestors opinion on any matter regarding the aragalaya',
    'அரகலயா தொடர்பான எந்தவொரு விஷயத்திலும் எதிர்ப்பாளர்களின் கருத்தை அறிய தேர்தலை அழைக்கவும்'
  ],
  urlLabel: [
    'මැතිවරණයෙහි දිගුව :',
    'Election url :',
    'தேர்தல் url :'
  ]
}
