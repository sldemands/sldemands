import { Component, OnInit } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { Router } from '@angular/router';
import { DatastoreService } from '../../services/datastore.service';
import { DatabaseService } from '../../services/database.service';
import { EventService } from '../../services/event.service';
import { HttpService } from '../../services/http.service';
import { ElectionStrings } from './strings';
import { EditorConfig } from '../../configs/editor';

@Component({
  selector: 'app-election',
  templateUrl: './election.component.html',
  styleUrls: ['./election.component.css']
})
export class ElectionComponent implements OnInit {

  editorConfig = EditorConfig;

  language: number = 0;
  strings = ElectionStrings;

  electionTitle: string = '';
  titleError: boolean = false;
  electionDescription: string = '';
  descriptionError: boolean = false;
  nominationCount: number = 100;
  nominationError: boolean = false;
  voteCount: number = 1;
  voteError: boolean = false;
  url: string = '';
  urlError: boolean = false;

  disableButton: boolean = false;

  constructor(
    private dbService: DatabaseService,
    private router: Router,
    private eventService: EventService,
    private datastore: DatastoreService,
    private httpService: HttpService
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {}

  goBack() {
    this.router.navigate(['']);
  }

  selectLanguage(val: number) {
    this.datastore.setLanguage(val);
  }

  nominationCountChange() {
    console.log(this.nominationCount);

    if(this.nominationCount > 100) this.nominationCount = 100;
    if(this.nominationCount < 2) this.nominationCount = 2;
    console.log(this.nominationCount);
  }

  voteCountChange() {
    if(this.voteCount > 10) this.voteCount = 10;
    if(this.voteCount < 1) this.voteCount = 1;
  }

  urlChange() {
    this.url = this.url.replace(/[^a-zA-Z0-9-_]/g, '').toLowerCase();
  }

  async callElection() {
    if(this.disableButton) return;
    this.disableButton = true;

    if(this.electionTitle == '') {
      this.disableButton = false;
      this.titleError = true;
    }
    if(this.electionDescription == '') {
      this.disableButton = false;
      this.descriptionError = true;
    }
    if(this.titleError || this.descriptionError) return;

    let auth = getAuth();
    if(!auth.currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
      this.disableButton = false;
      return;
    } else {
      this.eventService.emitEvent('show-progress', true);
      await this.httpService.createElection({
        title: this.electionTitle,
        description: this.electionDescription,
        maxNominations: this.nominationCount,
        maxVotes: this.voteCount,
        url: this.url,
        votes: 0,
        createdOn: (new Date()).getTime(),
        createdBy: auth.currentUser.uid
      });
      this.router.navigate(['polls',this.url])
      this.eventService.emitEvent('show-progress', false);
    }
    this.disableButton = false;
  }
}
