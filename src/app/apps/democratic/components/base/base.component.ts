import { Component, OnInit, EventEmitter, Output, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { EventService } from '../../../../services/event.service';
import { DatastoreService } from '../../../../services/datastore.service';
import { getAuth, signOut } from "firebase/auth";
import { BaseStrings } from '../../strings'

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit, OnChanges, OnDestroy {

  @Input() election: any;
  votesLeft: number = 1;
  selected: number = 0;
  mode: number = 0;
  loggedIn: boolean = false;
  language: number = 0;
  strings = BaseStrings;
  userSubscription: any;
  languageSubscription: any;
  user: any;

  constructor(
    private router: Router,
    private userService: UserService,
    private eventService: EventService,
    private datastore: DatastoreService
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.datastore.getUser().subscribe(user => {
      if(user) {
        this.user = user;
        this.loggedIn = true;
        this.updateVotes();
      }
    });
    this.languageSubscription = this.datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['election'].currentValue.title) {
      this.updateVotes();
    }
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.languageSubscription.unsubscribe();
  }

  updateVotes() {
    if(this.user) {
      let votes = 0;
      this.user.votedFor.map((vote: any) => {
        if(vote.election == this.election.url) votes++;
      });
      this.votesLeft = this.election.maxVotes - votes;
    }
  }

  select(val: number) {
    this.selected = val;
  }

  selectLanguage(val: number) {
    this.language = val;
    this.datastore.setLanguage(val);
  }

  login() {
    if(!this.loggedIn) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      signOut(getAuth()).then(() => {
        this.loggedIn = false;
        this.datastore.setUser(undefined);
      });
    }
  }

  goBack() {
    this.router.navigate(['']);
  }
}
