import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { getAuth } from 'firebase/auth';
import { UserService } from '../../../../services/user.service';
import { EventService } from '../../../../services/event.service';
import { DatabaseService } from '../../../../services/database.service';
import { DatastoreService } from '../../../../services/datastore.service';
import { HttpService } from '../../../../services/http.service';
import { CreateStrings } from '../../strings';
import { EditorConfig } from '../../../../configs/editor';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit, OnChanges {

  editorConfig = EditorConfig;

  @Input() election: any;
  itemTitle: string = '';
  titlePlaceholder: string = '';
  titleError: boolean = false;
  itemDescription: string = '';
  descriptionPlaceholder: string = '';
  descriptionError: boolean = false;
  disableButton: boolean = false;
  strings = CreateStrings;
  language: number = 0;
  maximized = false;

  constructor(
    private dbService: DatabaseService,
    private userService: UserService,
    private eventService: EventService,
    private datastore: DatastoreService,
    private httpService: HttpService
  ) {
    datastore.getLanguage().subscribe(val => this.language = val);
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  async addItem() {
    if(!this.maximized) {
      this.maximized = true;
      return;
    }

    if(this.disableButton) return;
    this.disableButton = true;

    if(this.itemTitle == '') {
      this.titleError = true;
      this.disableButton = false;
      return;
    }
    if(this.itemDescription == '') {
      this.descriptionError = true;
      this.disableButton = false;
      return;
    }

    let auth = getAuth();
    if(!auth.currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
      this.disableButton = false;
      return;
    } else {
      try {
        let item = {
          title: this.itemTitle,
          description: this.itemDescription,
          createdBy: auth.currentUser.uid,
          createdOn: new Date().getTime(),
          election: this.election.url,
          votes: 0,
          votedBy: []
        };
        await this.httpService.createNomination(item);
        this.itemTitle = '';
        this.itemDescription = '';
        this.eventService.emitEvent('refresh-items');
        this.eventService.emitEvent('show-message', 'ඇතුලත් කරන ලදී');
      } catch (error) {
        console.log(error);
      }
    }
    this.disableButton = false;
  }

  expandCreate() {
    if(!getAuth().currentUser && !this.maximized) {
      this.eventService.emitEvent('show-hide-login', true);
      return;
    } else {
      this.maximized = !this.maximized;
    }
  }
}
