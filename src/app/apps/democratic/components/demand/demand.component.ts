import { Component, OnInit, OnDestroy, Input, NgZone } from '@angular/core';
import { Item, UserRecord } from '../../../../types/types';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { UserService } from '../../../../services/user.service';
import { EventService } from '../../../../services/event.service';
import { DatabaseService } from '../../../../services/database.service';
import { DatastoreService } from '../../../../services/datastore.service';
import { HttpService } from '../../../../services/http.service';
import { DemandsStrings } from '../../strings';

@Component({
  selector: 'app-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.css']
})
export class DemandComponent implements OnInit, OnDestroy {

  @Input()
  item: Item = {
    title: '',
    description: '',
    votes: 0
  };
  @Input()
  totalVotes: number;
  voted: boolean = false;
  expanded: boolean = false;
  deletable: boolean = false;
  disableButton: boolean = false;

  strings = DemandsStrings;

  language: number = 0;
  languageSubscription: any;
  userSubscription: any;

  constructor(
    private dbService: DatabaseService,
    private userService: UserService,
    private eventService: EventService,
    private datastore: DatastoreService,
    private httpService: HttpService
  ) {
    this.languageSubscription = datastore.getLanguage().subscribe(val => this.language = val);
  }

  ngOnInit(): void {
    this.userSubscription = this.datastore.getUser().subscribe(user => {
      if(user) {
        let votes = user.votedFor;
        if(votes?.find((vote: any) => (vote.nomination == this.item.id as string))) {
          this.voted = true;
        }
        if(this.item.createdBy == user.uid) {
          this.deletable = true;
        }
      } else {
        this.voted = false;
        this.deletable = false;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.languageSubscription.unsubscribe();
  }

  expand() {
    this.expanded = !this.expanded;
  }

  async vote() {

    if(this.disableButton) return;
    this.disableButton = true;

    let auth = getAuth();
    if(!auth.currentUser) {
      this.eventService.emitEvent('show-hide-login', true);
      this.disableButton = false;
      return;
    } else {
      try {
        let result = (await this.httpService.vote({nominationId: this.item.id as string})).data as any;
        if(result.code < 0) {
          this.datastore.setUser(await this.dbService.getUserRecord(auth.currentUser.uid) as UserRecord);
        }
        if(result.code == -1) {
          this.eventService.emitEvent('show-message', 'චන්දය ප්‍රකාශ කරන ලදී');
          this.voted = true;
          this.item.votes++;
        }
        if(result.code == -2) {
          this.eventService.emitEvent('show-message', 'චන්දය ඉවත් කරන ලදී');
          this.voted = false;
          this.item.votes--;
        }
        if(result.code == 1) {
          this.eventService.emitEvent('show-message', 'ඔබට තවත් චන්ද නොමැත');
        }
        if(result.code > 1) {
          console.log(result.message);
        }
      } catch (error) {
        console.log(error);
      }
    }
    this.disableButton = false;
  }

  async deleteItem() {
    this.eventService.emitEvent('show-progress', true);
    await this.httpService.deleteNomination({nominationId: this.item.id as string});
    this.eventService.emitEvent('refresh-items');
    this.eventService.emitEvent('show-message', 'ඉල්ලීම ඉවත් කරන ලදී');
  }

}
