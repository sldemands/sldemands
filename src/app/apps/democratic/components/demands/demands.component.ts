import { Component, OnInit, OnChanges, OnDestroy, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { DatabaseService } from '../../../../services/database.service';
import { EventService } from '../../../../services/event.service';
import { UserService } from '../../../../services/user.service';
import { DatastoreService } from '../../../../services/datastore.service';
import { Item } from '../../../../types/types';
import { DemandsStrings } from '../../strings';
import { getAuth } from 'firebase/auth';

@Component({
  selector: 'app-demands',
  templateUrl: './demands.component.html',
  styleUrls: ['./demands.component.css']
})
export class DemandsComponent implements OnInit, OnChanges, OnDestroy {

  @Input() mode = 0;
  @Input() election: any;
  items: Item[] = [];
  showLoader: boolean = true;
  loaderContent: string = '...';
  selected:number = 1;
  selectedElection: number = 0;
  strings = DemandsStrings;

  language: number = 0;
  languageSubscription: any;
  user: any;
  userSubscription: any;

  elections: any[]= [];
  electionsSelector: string[] = [];

  votesLeft: number = 1;

  constructor(
    private dbService: DatabaseService,
    private eventService: EventService,
    private userService: UserService,
    private datastore: DatastoreService
  ) {
    this.languageSubscription = datastore.getLanguage().subscribe(val => this.language = val);
    this.userSubscription = datastore.getUser().subscribe(user => {
      if(user) {
        this.user = user;
        this.updateVotes();
      }
    });
  }

  ngOnInit(): void {
    this.election._previewDescription = true;
    this.dbService.getElections().then(elections => {
      this.elections = elections;
      this.selectedElection = this.elections.findIndex(e => (e.url==this.election.url));
    });
    this.eventService.registerEvent('refresh-items').subscribe(() => {
      this.getItems();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.election._previewDescription = true;
    if(changes['election'].currentValue.title) {
      this.getItems();
    }
    this.selected = 1;
    this.updateVotes();
  }

  ngOnDestroy() {
    this.languageSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  updateVotes() {
    if(this.user) {
      let votes = 0;
      this.user.votedFor.map((vote: any) => {
        if(vote.election == this.election.url) votes++;
      });
      this.votesLeft = this.election.maxVotes - votes;
    }
  }

  async getItems() {
    this.showLoader = true;
    if(this.selected == 1) {
      await this.dbService.getItemsByVotes(this.election.url).then(items => {
        this.items = items;
      });
    } else {
      if(!getAuth().currentUser) {
        this.eventService.emitEvent('show-hide-login', true);
        this.selected = 1;
      } else {
        if(this.selected == 2) {
          await this.dbService.getYourItems(this.election.url).then(items => {
            this.items = items;
          });
        }
        if(this.selected == 3) {
          await this.dbService.getVotedItems(this.election.url).then(items => {
            this.items = items;
          });
        }
      }
    }
    this.showLoader = false;
  }

  selectElection() {
    this.eventService.emitEvent('election-changed', this.elections[this.selectedElection].url);
  }
}
