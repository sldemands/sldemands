import { LanguageStrings } from '../../types/types';

export const DemandsStrings: LanguageStrings = {
  highestVotes: [
    'වැඩි ඡන්ද අනුව',
    'Highest Votes',
    'அதிக வாக்குகள்'
  ],
  yourDemands: [
    'ඔබේ යෝජනා',
    'Your suggestions',
    'உங்கள் பரிந்துரைகள்'
  ],
  yourVotes: [
    'ඔබ ඡන්දය දුන්',
    'Your votes',
    'உங்கள் வாக்குகள்'
  ],
  electionLabel: [
    'මත විමසීම: ',
    'Poll: ',
    'கருத்துக் கணிப்பு: '
  ],
  voteButton: [
    'එකඟයි',
    'Agree',
    'உடன்பாடு'
  ],
  suggestionDescription: [
    'සවිස්තරාත්මකව හේතු සහ අරමුණු :',
    'Reasons and motives :',
    'காரணங்கள் மற்றும் நோக்கங்கள் :'
  ],
  remainingVotes: [
    'ඔබට මෙම මත විමසීම සඳහා තවත් ඡන්ද *count* ක් ඉතිරිව ඇත.',
    'You have *count* votes left for this poll.',
    'இந்தத் தேர்தலில் உங்களுக்கு இன்னும் *count* வாக்குகள் உள்ளன'
  ]
}

export const CreateStrings: LanguageStrings = {
  suggest: [
    'යෝජනාවක් ඇතුල් කරන්න',
    'Add nomination',
    'நியமனத்தைச் சேர்க்கவும்'
  ],
  suggestionTitle: [
    'යෝජනාව',
    'Suggestion',
    'பரிந்துரை'
  ],
  suggestionDescription: [
    'සවිස්තරාත්මකව හේතු සහ අරමුණු',
    'Reasons and motives',
    'காரணங்கள் மற்றும் நோக்கங்கள்'
  ]
}

export const BaseStrings: LanguageStrings = {
  democraticTitle: [
    'මත විමසීම්',
    'Polls',
    'கருத்துக் கணிப்பு'
  ],
  directDemocracy: [
    'සෘජු ප්‍රජාතන්ත්‍රවාදය',
    'Direct democracy',
    'நேரடி ஜனநாயகம்'
  ],
  backButton: [
    'ආපසු',
    'Back',
    'திரும்பி வா'
  ],
  appTitle: [
    'අපේ ඉල්ලීම්!',
    'Our Demands!',
    'எங்கள் கோரிக்கைகள்!'
  ],
  demandsButton: [
    'අපේ ඉල්ලීම්!',
    'Our demands!',
    'எங்கள் கோரிக்கைகள்!'
  ],
  leadersButton: [
    'නායකයින්',
    'Leaders',
    'தலைவர்கள்'
  ],
  remainingVotes: [
    'ඔබට මෙම මත විමසීම සඳහා තවත් ඡන්ද *count* ක් ඉතිරිව ඇත.',
    'You have *count* votes left for this poll.',
    'இந்தத் தேர்தலில் உங்களுக்கு இன்னும் *count* வாக்குகள் உள்ளன'
  ],
  partners: [
    'සගයන් :',
    'Comrades :',
    'தோழர்கள் :'
  ]
}
