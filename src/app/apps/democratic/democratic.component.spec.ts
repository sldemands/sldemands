import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemocraticComponent } from './democratic.component';

describe('DemocraticComponent', () => {
  let component: DemocraticComponent;
  let fixture: ComponentFixture<DemocraticComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemocraticComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemocraticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
