import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { EventService } from '../../services/event.service';
import { DatabaseService } from '../../services/database.service';
import { DatastoreService } from '../../services/datastore.service';
import { BaseStrings } from './strings';
import { getAuth, signOut } from "firebase/auth";

@Component({
  selector: 'app-democratic',
  templateUrl: './democratic.component.html',
  styleUrls: ['./democratic.component.css']
})
export class DemocraticComponent implements OnInit {

  language: number = 0;

  election: string;
  electionData: any = {title: '', description: ''};

  strings = BaseStrings;
  loggedIn = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private eventService: EventService,
    private dbService: DatabaseService,
    private datastore: DatastoreService
  ) {
    this.eventService.registerEvent('election-changed').subscribe((election) => {
      this.electionChanged(election)
    });
    datastore.getUser().subscribe(user => (this.loggedIn = !!user));
  }

  ngOnInit(): void {
    this.route.url.subscribe(async (paths) => {
      this.election = paths[1].toString();
      this.updateElectionData();
    })
  }

  async updateElectionData() {
    this.electionData = await this.dbService.getElection(this.election);
  }

  electionChanged(name: string) {
    this.election = name;
    this.updateElectionData();
  }

  goBack() {
    this.router.navigate(['']);
  }

  login() {
    if(!this.loggedIn) {
      this.eventService.emitEvent('show-hide-login', true);
    } else {
      signOut(getAuth()).then(() => {
        this.loggedIn = false;
        this.datastore.setUser(undefined);
      });
    }
  }
}
