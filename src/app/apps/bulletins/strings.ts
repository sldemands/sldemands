import { LanguageStrings } from '../../types/types';

export const BulletinStrings: LanguageStrings = {
  bulletinsTitle: [
    'දැන්වීම් පුවරුව',
    'Bulletin Board',
    'அறிவிப்பு பலகை'
  ],
  backButton: [
    'ආපසු',
    'Back',
    'திரும்பி வா'
  ],
  verifiedLabel: [
    'නිල් වර්ණයෙන් දැක්වෙන්නේ තහවුරු කරන ලද දැන්වීමකි.',
    'Verified bulletins are shown in this blue.',
    'சரிபார்க்கப்பட்ட புல்லட்டின்கள் இந்த நீல நிறத்தில் காட்டப்பட்டுள்ளன.'
  ],
  bulletinTitle: [
    'මාතෘකාව ',
    'Title',
    'தலைப்பு'
  ],
  bulletinDescription: [
    'සවිස්තරාත්මක නිවේදනය',
    'Detailed notice',
    'விரிவான அறிவிப்பு'
  ],
  bulletinSubmit: [
    'ඇතුල් කරන්න',
    'Post',
    'வை'
  ]
}
