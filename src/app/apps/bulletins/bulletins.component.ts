import { Component, OnInit } from '@angular/core';
import { Bulletin } from '../../types/types';
import { BlankBulletin } from '../../components/bulletin/bulletin.component';
import { BulletinStrings } from './strings';
import { Router } from '@angular/router';
import { DatastoreService } from '../../services/datastore.service';
import { DatabaseService } from '../../services/database.service';
import { EventService } from '../../services/event.service';
import { getAuth } from 'firebase/auth';

@Component({
  selector: 'app-bulletins',
  templateUrl: './bulletins.component.html',
  styleUrls: ['./bulletins.component.css']
})
export class BulletinsComponent implements OnInit {

  language: number = 0;
  strings = BulletinStrings;

  bulletins: Bulletin[] = [BlankBulletin, BlankBulletin];

  showLoader: boolean = false;
  loaderContent: string = '...';

  constructor(
    private dbService: DatabaseService,
    private router: Router,
    private eventService: EventService,
    private datastore: DatastoreService,
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
    this.getBulletins();
    this.eventService.registerEvent('reload-bulletins').subscribe(() => {
      this.showLoader = true;
      this.getBulletins();
    })
  }

  getBulletins() {
    this.dbService.getBulletins().then(bulletins => {
      this.bulletins = bulletins.sort((a,b) => (b.createdOn - a.createdOn));
      this.showLoader = false;
    })
  }

  goBack() {
    this.router.navigate(['']);
  }

  selectLanguage(val: number) {
    this.datastore.setLanguage(val);
  }

}
