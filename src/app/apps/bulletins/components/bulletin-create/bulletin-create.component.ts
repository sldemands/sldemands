import { Component, OnInit, Input } from '@angular/core';
import { getAuth } from 'firebase/auth'
import { UserService } from '../../../../services/user.service';
import { EventService } from '../../../../services/event.service';
import { DatabaseService } from '../../../../services/database.service';
import { DatastoreService } from '../../../../services/datastore.service';
import { HttpService } from '../../../../services/http.service';
import { BulletinStrings } from '../../strings';
import { EditorConfig } from '../../../../configs/editor';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-bulletin-create',
  templateUrl: './bulletin-create.component.html',
  styleUrls: ['./bulletin-create.component.css']
})
export class BulletinCreateComponent implements OnInit {

  editorConfig = EditorConfig;

  maximized: boolean = false;

  strings = BulletinStrings;
  language: number = 0;

  itemTitle: string = '';
  titleError: boolean = false;
  itemDescription: string = '';
  descriptionError: boolean = false;
  disableButton: boolean = false;

  constructor(
    private dbService: DatabaseService,
    private userService: UserService,
    private eventService: EventService,
    private datastore: DatastoreService,
    private httpService: HttpService
  ) {
    datastore.getLanguage().subscribe(val => (this.language = val));
  }

  ngOnInit(): void {
  }

  async submitBulletin() {
    if(this.disableButton) return;
    this.disableButton = true;

    if(this.itemTitle == '') {
      this.titleError = true;
    }
    if(this.itemDescription == '') {
      this.descriptionError = true;
    }

    if(this.titleError || this.descriptionError) {
      this.disableButton = false;
      return;
    }

    let userRecord = await firstValueFrom(this.datastore.getUser());
    if(userRecord) {
      await this.httpService.createBulletin({
        title: this.itemTitle,
        description: this.itemDescription,
        verified: !!userRecord.verified,
        createdOn: new Date().getTime(),
        createdByName: userRecord.name,
        createdBy: userRecord.uid
      })
      this.itemTitle = '';
      this.itemDescription = '';
      this.eventService.emitEvent('reload-bulletins');
      this.maximized = false;
    }
    this.disableButton = false;
  }

  expandCreate() {
    if(!getAuth().currentUser && !this.maximized) {
      this.eventService.emitEvent('show-hide-login', true);
      return;
    } else {
      this.maximized = !this.maximized;
    }
  }
}
